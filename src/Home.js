import React from 'react';
import Header from './Header/Header';
import { Routes, Route } from 'react-router-dom';

import Halamanjumlah from './component/Halamanjumlah';
import Pesanbarang from './component/Pesanbarang';
import TotalTransaksi from './component/TotalTransaksi';

// membuat global state manajement
import GlobalProvider from './context/context';

// import { createContext } from 'react';
import { Component } from 'react';



class Home extends Component {
    render(){
        return (
            <div>
                <Header />
                <Routes>
                    <Route path='/' element={<Halamanjumlah />} />
                    <Route path='/page-pesanbarang' element={<Pesanbarang />} />
                    <Route path='/page-totaltransaksi' element={<TotalTransaksi />} />
                </Routes>
            </div>
        );
    }
}

export default GlobalProvider(Home);
















// export const RootContext = createContext();
// const Provider = RootContext.Provider;

// class Home extends Component {
//     state = {
//         totalOrder: 5   
//     }

//     handleChangeState = (action) => {
//         if(action.type === 'PLUS_OLDER'){
//             return  this.setState({
//                 totalOrder: this.state.totalOrder + 1
//             })
//         }
//         if(action.type === 'MINUS_OLDER'){
//             return this.setState({
//                 totalOrder: this.state.totalOrder - 1
//             })
//         }
//     }

//     render(){   
//     return (
//         <div>
//             <Header />
//             <Provider value={
//                 {
//                     state: this.state,
//                     handleChangeState: this.handleChangeState
//                 }
//                 }>
//                 <Routes>
//                     <Route path='/' element={<Halamanjumlah />} />
//                     <Route path='/page-pesanbarang' element={<Pesanbarang />} />
//                     <Route path='/page-totaltransaksi' element={<TotalTransaksi />} />
//                 </Routes>
//             </Provider>
//         </div>
//     );
//     }
// }

// export default Home;