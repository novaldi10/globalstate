import React, { Children, Component, createContext } from 'react';



const RootContext = createContext();


// Provider
const Provider = RootContext.Provider;
const GlobalProvider = (Children) => {

    return (
        class ParentComp extends Component {
            state = {
                totalOrder: 5,  
                quantyty: 1,
                hargaJam: 200000,
                totalHarga: 200000,
            }

            handleChangeState = (action) => {
                if(action.type === 'PLUS_OLDER'){
                    return  this.setState({
                        totalOrder: this.state.totalOrder + 1
                    })
                }
                if(action.type === 'MINUS_OLDER'){
                    return this.setState({
                        totalOrder: this.state.totalOrder - 1
                        // totalHarga: this.hitungTotalHarga(),
                    })
                }
            }

            tambahJumlah = () => {
                this.setState(prevState => {
                    return{
                        quantyty: prevState.quantyty + 1,
                        totalHarga: (prevState.quantyty + 1) * prevState.hargaJam
                    }
                }) 
            }

            kurangiJumlah = () => {
                if (this.state.quantyty > 1) {
                    this.setState(prevState => {
                        return{
                            quantyty: prevState.quantyty - 1,
                            totalHarga: (prevState.quantyty - 1) * prevState.hargaJam 
                        }
                    })
                }
            }

            // hitungTotalHarga = () => {
            //     return this.state.quantyty * this.state.hargaJam;
            // }

            // updateQuantyty = (amount) => {
            //     this.setState(prevState => ({
            //         quantyty: prevState.quantyty + amount,
            //         totalHarga: this.hitungTotalHarga()
            //     }))
            // }

            // updateHarga = (event) => {
            //     this.setState({
            //         harga: parseInt(event.target.value),
            //         totalHarga: this.hitungTotalHarga()
            //     })
            // }

            render(){
              return(
                  <Provider value={
                    {
                        state: this.state,
                        handleChangeState: this.handleChangeState,
                        quantyty: this.state.quantyty,
                        hargaJam: this.state.hargaJam,
                        totalHarga: this.state.totalHarga,
                        tambahJumlah: this.tambahJumlah,
                        kurangiJumlah: this.kurangiJumlah,
                        // updateQuantyty: this.updateQuantyty,
                        // updateHarga: this.updateHarga,
                    }
                  }>
                      {/* props dichildren digunakan untuk menghendle jika props dibutuhkan */}
                       <Children {...this.props}/>
                  </Provider>
                )
            }  
        }
    )
}

export default GlobalProvider;


// Consumer
// karena dalam satu file hanya bisa menggunakan satu export default diakhir codingan maka export digunakan didepan const global
const Consumer = RootContext.Consumer;
export const GlobalConsumer = (Children) => {
    return(
        class ParentConsumer extends Component {
            render(){
                return(
                    <Consumer>
                        {
                            value => {
                                return (
                                    <Children {...this.props} {...value} />
                                )
                            }
                        }
                    </Consumer>
                )
            }
        }
        
    )
}

