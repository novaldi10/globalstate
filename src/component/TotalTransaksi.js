import React, { Component } from 'react';

import { Avatar, Button, CardContent, Checkbox, FormControl, FormControlLabel, InputLabel, MenuItem, Select, Typography } from '@mui/material';
import Box from '@mui/material/Box';
import { GlobalConsumer } from '../context/context';

import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';

import Jam from '../assets/jam tangan.jpg';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import Stack from '@mui/material/Stack';

import { Snackbar } from '@mui/material';
import { Alert } from '@mui/material';
import Container from '@mui/material/Container';


function SnackbarAlert(props) {
    return <Alert elevation={6} variant="filled" {...props} />;
}

class  TotalTransaksi extends Component {

    state = {
        checked: false,
        open: false,
        selectedCountry: '',
        countries: [
            { name: 'Indonesia', code: 'ID' },
            { name: 'Malaysia', code: 'MY' },
            { name: 'Singapore', code: 'SG' },
            { name: 'Thailand', code: 'TH' },
            { name: 'Vietnam', code: 'VN' },
        ]
    }

    // membuat tombol pilihan
    handleCountryChange = (event) => {
        this.setState({ selectedCountry: event.target.value });
    }

    // membuat tombol chackbox
    handleChecked = (event) => {
        this.setState({ checked: event.target.checked })
    }


    handleClick = () => {
        this.setState({ open: true });
    }

    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
    }

    this.setState({ open: false });
    }   

    render(){
        const { open } = this.state;

    return (
        <Container maxWidth="xl">
            <Typography variant="h6">Billing Details</Typography> 


            <Box sx={{display: 'flex', justifyContent: 'space-between'}}>
           
            <Grid container spacing={3}>

                <Grid item>
                    <Typography>EMAIL</Typography>
                    <TextField 
                        id="outlined-basic" 
                        variant="outlined" 
                        placeholder="Enter your email address"
                        maxRows={1}
                        InputProps={{style: {
                            width: 400
                        }}}
                    />
                </Grid>

                <Grid item>
                    <Typography>COMPANY NAME </Typography>
                    <TextField 
                        id="outlined-basic" 
                        variant="outlined" 
                        placeholder="Enter company address"
                        maxRows={1}
                        InputProps={{style: {
                            width: 400
                        }}}
                    />
                </Grid>

                <Grid item>
                    <Typography>VAT NUMBER </Typography>
                    <TextField 
                        id="outlined-basic" 
                        variant="outlined" 
                        placeholder="Enter VAT number"
                        maxRows={1}
                        InputProps={{style: {
                            width: 400
                        }}}
                    />
                </Grid>

                <Grid item>
                    <Typography>BILLING ADDRESS </Typography>
                    <TextField 
                        id="outlined-basic" 
                        variant="outlined" 
                        placeholder="Enter your billing address"
                        maxRows={1}
                        InputProps={{style: {
                            width: 400
                        }}}
                    />
                </Grid>

                <Grid item>
                    <Typography>ZIP CODE </Typography>
                    <TextField 
                        id="outlined-basic" 
                        variant="outlined" 
                        placeholder="Enter Code"
                        maxRows={1}
                        InputProps={{style: {
                            width: 400
                        }}}
                    />
                </Grid>

                <Grid item>
                    <Typography>CITY </Typography>
                    <TextField 
                        id="outlined-basic" 
                        variant="outlined" 
                        placeholder="Enter City"
                        maxRows={1}
                        InputProps={{style: {
                            width: 400
                        }}}
                    />
                </Grid>

                <Grid item>
                    <FormControl sx={{width: 400}}>
                        <InputLabel id="country-select-label">Country</InputLabel>
                        <Select
                            labelId="country-select-label"
                            id="country-select"
                            value={this.state.selectedCountry}
                            onChange={this.handleCountryChange}
                            label="Country"
                        >
                        <MenuItem value="">--Country--</MenuItem>
                        {this.state.countries.map((country) => (
                            <MenuItem key={country.code} value={country.name}>
                                {country.name}
                            </MenuItem>
                        ))}
                        </Select>
                    </FormControl>
                </Grid>

            </Grid>

            <Box sx={{display: 'flex', mr: 10}}>
            <Card sx={{ maxWidth: 400, width: 390, bgcolor: "#E1EEDD"}}>
                <CardContent>
                    <Typography variant="h6">Order Summary</Typography>
                    <Stack direction="row" spacing={1}>
                        <Avatar src={Jam} sx={{ width: 56, height: 56 }}/>
                        <Typography sx={{pt: 1}}>{this.props.state.quantyty}x Mono Clock <br/> SKU: 0636980636627</Typography>
                    </Stack>
                </CardContent>
                <CardActions>
                    <Stack direction="row" spacing={15}>
                        <Typography>Total Price <br/> Rp. {this.props.state.totalHarga}</Typography>
                            <Button onClick={this.handleClick}
                            sx={{
                                width: 150,
                                color: "grey",
                                backgroundColor: "#8BF5FA",
                                "&:hover": {
                                    backgroundColor: "#3F979B"
                                }
                            }}
                            >
                            Pay
                            </Button>

                            <Snackbar
                                open={open}
                                autoHideDuration={1500}
                                onClose={this.handleClose}
                                anchorOrigin={{ vertical: 'center', horizontal: 'center' }}
                                sx={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    height: "100%"
                                }}
                            >  
                            <Alert onClose={this.handleClose} severity='success'>
                                Verifikasi sudah dibayarkan
                            </Alert>
                            </Snackbar>
                        </Stack>
                </CardActions>
            </Card>
            </Box>
            </Box>

            <Box pt={2}>
                <Typography variant="h6">Dilivery info</Typography>
                <FormControlLabel
                    control={
                        <Checkbox 
                            checked={this.state.checked}
                            onChange={this.handleChecked}
                            nama="checked"
                            color="primary"
                        />
                    }
                    label="Same as billing address"
                />

            </Box>
        </Container>
    );
}
}

export default GlobalConsumer(TotalTransaksi);

// const SnackbarAlert = forwardRef<HTMLDivElement, AlertProps>(
//     function SnackbarAlert(props, ref) {
//         return <Alert elevation={6} ref={ref} {...props}/>
//     }
// )
