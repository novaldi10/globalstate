import React from 'react';
import { TableContainer, Table, TableHead, TableBody, TableRow, TableCell } from '@mui/material';
import NoteOutlinedIcon from '@mui/icons-material/NoteOutlined';
import Stack from '@mui/material/Stack';
import Pagination from '@mui/material/Pagination';
import { Container } from '@mui/system';

// bagian ngubah tanggal (iconnya)
import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import { TextField } from '@mui/material';
import { useState } from 'react';

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import { Typography } from '@mui/material';
import { Paper } from '@mui/material';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';

import Avatar from '@mui/material/Avatar';
import jam1 from '../assets/jam1.jpg';
import jam2 from '../assets/jam2.jpg';
import jam3 from '../assets/jam3.jpg';

import { GlobalConsumer } from '../context/context';
import { Component } from 'react';

// membuat link ke halaman lain
import { Link } from 'react-router-dom';

class Pesanbarang extends Component {
  render(){
    return (
        <Container maxWidth="xl">
        {/* Bagian Select Bulan */}
        <Box display='flex' justifyContent='center' pt={2}>
            <Box>
                <Button 
                // onClick={handleBackButtonClick} 
                variant="contained" sx={{backgroundColor: '#313541', width: 40, height: 40}}>
                <KeyboardArrowLeftIcon fontSize="medium"/>
                </Button>
            </Box>
            <Box>
                <TextField
                  // value={filter}
                  // onChange={e => setFilter(e.target.value)}
  
                  id="outlined-read-only-input"
                  defaultValue="Desember 2022"
                  InputProps={{  
                      inputProps: { 
                        style: { textAlign: "center", width: 269, height: 8 }
                    }
                  }}
                  sx={{backgroundColor: 'white', color: 'black'}}
                />
            </Box>
            <Box>
                <Button 
                // onClick={handleNextButtonClick} 
                variant="contained" sx={{backgroundColor: '#313541', width: 40, height: 40}}>
                <KeyboardArrowRightIcon fontSize="medium"/>
                </Button>
            </Box>
        </Box>
        <Divider sx={{borderColor: 'white', pt: 2}}/> 
  
  
        {/* mumbuat table */}
        <TableContainer component={Paper}>
        <Table arial-label='simple table'>
            <TableHead sx={{backgroundColor: '#303F51'}}>
                <TableRow>
                    <TableCell sx={{color: 'white'}}>Produk</TableCell>
                    <TableCell sx={{color: 'white'}} align='center'>Harga</TableCell>
                    <TableCell sx={{color: 'white'}} align='center' >Quantyty</TableCell>
                    <TableCell sx={{color: 'white'}} align='center'>Total</TableCell>
                    <TableCell sx={{color: 'white'}} align='center'>Order Summary</TableCell>

                </TableRow>
            </TableHead>
            <TableBody>
                {tableData.map(row => (
                    <TableRow 
                        key={row.nomor}
                        sx={{'&:last-child td, &:last-child th' : { border: 0}}}
                    >
                        <TableCell align='left' sx={{width: 157}}>
                          <Stack direction="row" spacing={2}>
                              {row.produk} <br />
                              {row.pesanan}
                          </Stack>
                        </TableCell>

                        <TableCell align='center' sx={{width: 300}}>{row.harga}</TableCell>

                        <TableCell align='center' >
                          <Button disabled={this.props.state.quantyty < 1 ? true : false} onClick={this.props.kurangiJumlah} sx={{height: 41, width: 20}}>
                            <Typography sx={{color: 'black'}}>-</Typography>
                          </Button>
                          <TextField
                              value={this.props.state.quantyty}
                              id="outlined-read-only-input"
                              InputProps={{
                                  inputProps: {
                                      style: { textAlign: "center", width: 30, height: 8 }
                                  }
                              }}
                              sx={{ backgroundColor: 'white', color: 'black' }}
                          />
                          <Button onClick={this.props.tambahJumlah} sx={{height: 41, width: 20}}>
                          <Typography sx={{color: 'black'}}>+</Typography>
                          </Button>
                        </TableCell>

                        <TableCell align='center' sx={{width: 300}}>{this.props.totalHarga}</TableCell>
                        
                        <TableCell align='center' bgColor='#28A745' sx={{width: 157}}>
                            
                            <Button
                                component={Link}
                                to='/page-totaltransaksi'
                                size='large'
                                // color="success"
                                sx={{
                                    mr: 1, 
                                    width: 110,
                                    height: '100%',
                                    "&:hover": { backgroundColor: "28A745"},
                                    fontFamily: 'roboto',
                                }}
                            >
                            <ShoppingCartOutlinedIcon  fontSize="large"/>
                            <Typography sx={{ ml: 1 , fontFamily: 'roboto', fontSize: 16, textAlign: 'left' }}>      
                            Beli
                            </Typography>
                            </Button> 
                
                        </TableCell>
                    </TableRow>
            
                ))
                }
            </TableBody>


            {/* table ke-2 */}
            <TableBody>
               {tableData1.map(row => (
                <TableRow>
                    <TableCell>{row.produk} <br/ > {row.pesanan}</TableCell>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                    <TableCell align='center' bgColor='#28A745' sx={{width: 157}}>

                     <Button
                       // variant="contained"
                       size='large'
                       // color="success"
                       sx={{
                         mr: 1,
                         width: 110,
                         height: '100%',
                         "&:hover": { backgroundColor: "28A745" },
                         fontFamily: 'roboto',
                       }}
                     >
                       <NoteOutlinedIcon fontSize="large" />
                       <Typography sx={{ ml: 1, fontFamily: 'roboto', fontSize: 10, textAlign: 'left' }}>
                         Lihat
                         Catatan
                       </Typography>
                     </Button>

                    </TableCell>
                </TableRow>
                ))}
            </TableBody>
        </Table>
    </TableContainer>
  
    <Button component={Link} to='/' variant="contained" sx={{Color: '#808080'}}>Kembali</Button>

    <Box pt={10}>
    <Stack spacing={2} alignItems='center'>
        <Pagination 
        //  count={5} 
            shape="rounded"
            // count={Math.ceil(tableData.length / rowsPerPage)}
            // page={page}
            // onChange={handleChange}
            sx={{
                "& .MuiPaginationItem-page": { color: "green", border: "1px solid green" },
                "& .Mui-selected": { backgroundColor: "#00FF00" },
                "& .MuiPagination-ul li:last-child button::before": {content: "'Next'", marginRight: "12px", color: 'green'},
                "& .MuiPagination-ul li:first-child button::after": {content: "'Prev'", marginLeft: "12px", color: 'green'},
            }}
        />
    </Stack>
    </Box> 
    </Container>
    );
  }
}

export default GlobalConsumer(Pesanbarang);

const tableData = [{
    nomor: "1",
    produk: "Mono Clock",
    pesanan: "SKU: 0636980636627",
    harga: "Rp. 200.000",
    gambar: {jam1},
  }]

const tableData1 = [{
  nomor: "2",
  produk: "15 Minute Timer",
  pesanan: "SKU: 0636980636627",
  harga: "Rp. 150.000",
  gambar: {jam2},
}]

const tableData2 = [{
  nomor: "3",
  produk: "Kitty Clock",
  pesanan: "SKU: 0636980636627",
  harga: "Rp. 230.000",
  gambar: {jam3},
}]