import React from 'react';
import { Typography } from '@mui/material'; 
import Box from '@mui/material/Box';
import { createTheme , ThemeProvider } from '@mui/material';
import { grey } from '@mui/material/colors';
import Button from '@mui/material/Button';
import { TextField } from '@mui/material';

import { Link } from 'react-router-dom';

import { Component } from 'react';
import { GlobalConsumer } from '../context/context';

// untuk tampilan gambar olshop
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Jam from '../assets/jam tangan.jpg';
import Jam1 from '../assets/jam1.jpg';
import Grid from '@mui/material/Grid';


const theme = createTheme({
  palette: {
      primary: {
          main : grey[200],
      },
      // primary: lightBlue,
      // secondary: pink,
  }
});

class Halamanjumlah extends Component {
    render(){
        console.log(this);
        return (
            <ThemeProvider theme={theme}>
                <Box bgcolor='#303F51'>

                    <Grid container spacing={1}>
                    
                    <Grid item>
                    <Card>
                        <CardContent>
                            <Typography>Mono Clock</Typography>
                            <Box component={'img'} src={Jam}/>
                        </CardContent>
                    </Card>
                    </Grid>
                    
                    <Grid item>
                    <Card>
                        <CardContent>
                        <Typography variant='h6'>Mono Clock</Typography>
                        <Typography>SKU: 0636980636627</Typography>
                        <Typography>The Mono Wall Clock is a simple, yet striking
                            <br /> 
                            timepiece, The perfectily black and white dial, <br /> 
                            along,  with the contrasting matte and glossy <br /> 
                            surfaces, give this clock a classic look. </Typography>
                        <Typography sx={{pt: 2}}>QUANTYTY</Typography>
                        </CardContent>

                        <CardActions>
                        {/* <Typography>{harga * quantyty}</Typography>
                        <Button disabled={quantyty < 1 ? true:false} onClick={()=>setquantyty(quantyty - 1)}>-</Button> */}
                        
                        <Button disabled={this.props.state.quantyty < 1 ? true : false} onClick={this.props.kurangiJumlah} sx={{height: 41, width: 20}}>
                        {/* <Button disabled={this.props.state.handleChangeState < 2 ? true : false} onClick={()=> this.props.handleChangeState({type: 'MINUS_OLDER'})} sx={{height: 41, width: 20}}> */}
                           <Typography sx={{color: 'black'}}>-</Typography>
                        </Button>
                        <TextField
                            // value={quantyty}
                            // onChange={e => setquantyty(e.target.value)}

                            value={this.props.state.quantyty}
                            // value={this.props.state.totalOrder}
                            id="outlined-read-only-input"
                            defaultValue="Desember 2022"
                            InputProps={{
                                inputProps: {
                                    style: { textAlign: "center", width: 30, height: 8 }
                                }
                            }}
                            sx={{ backgroundColor: 'white', color: 'black' }}
                        />
                        <Button onClick={this.props.tambahJumlah} sx={{height: 41, width: 20}}>
                        <Typography sx={{color: 'black'}}>+</Typography>
                        </Button>
                        {/* <Button onClick={()=>setquantyty(quantyty + 1)}>+</Button> */}
                        {/* <Typography sx={{ml: 5}}>Rp.{this.props.totalHarga}</Typography> */}
                        <Typography sx={{ml: 5}}>Rp.{this.props.totalHarga}</Typography>
                        </CardActions>
                    </Card>
                    <Button variant="contained" component={Link} to='/page-pesanbarang' sx={{width: 362, backgroundColor:'grey'}}>Tambahkan Ke Kartu</Button>
                    {/* <Button component={Link} to='/page-totaltransaksi'>Pesan</Button> */}
                    </Grid>

                    <Grid item>
                    <Card>
                        <CardContent>
                            <Typography>15 Minute Timer</Typography>
                            <Box component={'img'} src={Jam1}/>
                        </CardContent>
                    </Card>
                    </Grid>

                    {/* barang kedua */}
                    <Grid item>
                    <Card>
                        <CardContent>
                        <Typography variant='h6'>15 Minute Timer </Typography>
                        <Typography>SKU: 0636980636627</Typography>
                        <Typography>The Mono Wall Clock is a simple, elegand
                            <br /> 
                            timepiece, The perfectily red and white dial, <br /> 
                            along,  with the contrasting matte and glossy <br /> 
                            surfaces, give this clock a classic look. </Typography>
                        <Typography sx={{pt: 2}}>QUANTYTY</Typography>
                        </CardContent>

                        <CardActions>
                        {/* <Typography>{harga * quantyty}</Typography>
                        <Button disabled={quantyty < 1 ? true:false} onClick={()=>setquantyty(quantyty - 1)}>-</Button> */}
                        
                        <Button disabled={this.props.state.quantyty < 1 ? true : false} onClick={this.props.kurangiJumlah} sx={{height: 41, width: 20}}>
                        {/* <Button disabled={this.props.state.handleChangeState < 2 ? true : false} onClick={()=> this.props.handleChangeState({type: 'MINUS_OLDER'})} sx={{height: 41, width: 20}}> */}
                           <Typography sx={{color: 'black'}}>-</Typography>
                        </Button>
                        <TextField
                            // value={quantyty}
                            // onChange={e => setquantyty(e.target.value)}

                            value={this.props.state.quantyty}
                            // value={this.props.state.totalOrder}
                            id="outlined-read-only-input"
                            defaultValue="Desember 2022"
                            InputProps={{
                                inputProps: {
                                    style: { textAlign: "center", width: 30, height: 8 }
                                }
                            }}
                            sx={{ backgroundColor: 'white', color: 'black' }}
                        />
                        <Button onClick={this.props.tambahJumlah} sx={{height: 41, width: 20}}>
                        <Typography sx={{color: 'black'}}>+</Typography>
                        </Button>
                        {/* <Button onClick={()=>setquantyty(quantyty + 1)}>+</Button> */}
                        {/* <Typography sx={{ml: 5}}>Rp.{this.props.totalHarga}</Typography> */}
                        <Typography sx={{ml: 5}}>Rp.{this.props.totalHarga}</Typography>
                        </CardActions>
                    </Card>
                    <Button variant="contained" component={Link} to='/page-pesanbarang' sx={{width: 362, backgroundColor:'grey'}}>Tambahkan Ke Kartu</Button>
                    {/* <Button component={Link} to='/page-totaltransaksi'>Pesan</Button> */}
                    </Grid>


                    </Grid>
                </Box>
            </ThemeProvider>
        );
    }
}

export default GlobalConsumer(Halamanjumlah);

