import * as React from 'react';
import Pagination from '@mui/material/Pagination';
// import PaginationItem from '@mui/material/PaginationItem';
import Stack from '@mui/material/Stack';
import { Box } from '@mui/system';


export default function PaginationRounded() {
  return (
    <Box bgcolor='#303F51' pt={10}>
        <Stack spacing={2} alignItems='center'>
            <Pagination count={5} shape="rounded"
                sx={{
                    "& .MuiPaginationItem-page": { color: "white", border: "1px solid white" },
                    "& .Mui-selected": { backgroundColor: "#00FF00" },
                    "& .MuiPagination-ul li:last-child button::before": {content: "'Next'", marginRight: "12px", color: 'white'},
                    "& .MuiPagination-ul li:first-child button::after": {content: "'Prev'", marginLeft: "12px", color: 'white'},
                  }}
            />
        </Stack>

        <Box pt={4}>

        </Box>
    </Box>    
  );
}